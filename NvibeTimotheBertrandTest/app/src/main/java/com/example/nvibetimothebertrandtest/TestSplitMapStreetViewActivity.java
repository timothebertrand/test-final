package com.example.nvibetimothebertrandtest;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.model.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TestSplitMapStreetViewActivity extends FragmentActivity implements OnMapReadyCallback, Api.ApiOptions, StreetViewPanorama.OnStreetViewPanoramaChangeListener, GoogleMap.OnMarkerDragListener {

    private static final LatLng PARIS = new LatLng(48.8534,2.3488);
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private static final String TAG = MapsActivity.class.getName();
    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseUser curUser;

    private EditText mSearch, txtDesc;

    private Button btnSearch, btnConfirm;

    private StreetViewPanorama mStreetViewPanorama;

    private Marker mMarker;

    private LatLng markerPosition = PARIS;


    private void enableMyLocation() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            Permissions.Check_COARSE_LOCATION(this);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
            /*mMap.getUiSettings().setMyLocationButtonEnabled(false);*/
        }
    }

    @Override
    public void onStreetViewPanoramaChange(StreetViewPanoramaLocation location) {
        if (location != null && location.position != null) {
            mMarker.setPosition(location.position);
        }
    }


    private static class LongPressLocationSource implements LocationSource, GoogleMap.OnMapLongClickListener {

        private OnLocationChangedListener mListener;


        /**
         * Flag to keep track of the activity's lifecycle. This is not strictly necessary in this
         * case because onMapLongPress events don't occur while the activity containing the map is
         * paused but is included to demonstrate best practices (e.g., if a background service were
         * to be used).
         */
        private boolean mPaused;

        @Override
        public void activate(OnLocationChangedListener listener) {
            mListener = listener;
        }


        @Override
        public void deactivate() {
            mListener = null;
        }

        @Override
        public void onMapLongClick(LatLng point) {
            if (mListener != null && !mPaused) {
                Location location = new Location("LongPressLocationProvider");
                location.setLatitude(point.latitude);
                location.setLongitude(point.longitude);
                location.setAccuracy(100);
                mListener.onLocationChanged(location);
            }
        }


        public void onPause() {
            mPaused = true;
        }

        public void onResume() {
            mPaused = false;
        }
    }

    private LongPressLocationSource mLocationSource;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_split_map_street_view);

        mSearch = (EditText) findViewById(R.id.input_search);
        txtDesc = (EditText) findViewById(R.id.txtDesc);
        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        curUser = FirebaseAuth.getInstance().getCurrentUser();
        /*btnSearch = (ImageView) findViewById(R.id.im_magnify);*/

        mLocationSource = new LongPressLocationSource();

        btnConfirm.setVisibility(View.GONE);
        txtDesc.setVisibility(View.GONE);

        final LatLng markerPosition;
        if (savedInstanceState == null) {
            markerPosition = PARIS;
        } else {
            markerPosition = savedInstanceState.getParcelable("Marker position");
        }

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        CollectionReference fav = db.collection("favoris");
        Query query = fav.whereEqualTo("email", curUser.getEmail());

        SupportStreetViewPanoramaFragment streetViewPanoramaFragment =
                (SupportStreetViewPanoramaFragment)
                        getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                new OnStreetViewPanoramaReadyCallback() {
                    @Override
                    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                        mStreetViewPanorama = panorama;
                        mStreetViewPanorama.setOnStreetViewPanoramaChangeListener(TestSplitMapStreetViewActivity.this);

                        mMarker = mMap.addMarker(new MarkerOptions()
                                .position(markerPosition)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_gps))
                                .draggable(true));

                        // Only set the panorama to SYDNEY on startup (when no panoramas have been
                        // loaded which is when the savedInstanceState is null).
                        if (savedInstanceState == null) {
                            panorama.setPosition(PARIS);
                        }
                    }
                });

        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (!queryDocumentSnapshots.isEmpty()) {
                    for (int i = 0; i < queryDocumentSnapshots.size(); i++) {
                        GeoPoint point = (GeoPoint) queryDocumentSnapshots.getDocuments().get(i).get("location");
                        LatLng latLng = new LatLng((float) point.getLatitude(), (float) point.getLongitude());
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        mMap.addMarker(markerOptions);
                    }
                } else {
                    Toast.makeText(TestSplitMapStreetViewActivity.this, "No result found", Toast.LENGTH_LONG);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(TestSplitMapStreetViewActivity.this, "Bug", Toast.LENGTH_LONG);
            }
        });
        /*SupportMapFragment mapFragment2 =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment2.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                map.setOnMarkerDragListener(TestSplitMapStreetViewActivity.this);
                // Creates a draggable marker. Long press to drag.
                mMarker = map.addMarker(new MarkerOptions()
                        .position(markerPosition)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_gps))
                        .draggable(true));
            }
        });*/
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocationSource.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationSource.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("Marker", mMarker.getPosition());
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        mStreetViewPanorama.setPosition(marker.getPosition(), 150);
    }

    @Override
    public void onMarkerDrag(Marker marker) {
    }

    private void init() {
        Log.d(TAG, "init: initializing");


        mSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH
                        || i == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {

                    geoLocate();
                }
                return false;
            }
        });
        hideSoftKeyboard();

    }

    private void moveCamera(LatLng latLng, float zoom, String title) {


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title(title);
        mMap.addMarker(options);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                AlertDialog alertDialog;
                alertDialog = new AlertDialog.Builder(TestSplitMapStreetViewActivity.this)
                        .setMessage("Add to favorite?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                txtDesc.setVisibility(View.VISIBLE);
                                btnConfirm.setVisibility(View.VISIBLE);
                                btnConfirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        txtDesc.setVisibility(View.GONE);
                                        btnConfirm.setVisibility(View.GONE);
                                        Map<String, Object> location = new HashMap<>();
                                        location.put("email", curUser.getEmail());
                                        location.put("name", marker.getTitle());
                                        location.put("location", new GeoPoint(marker.getPosition().latitude, marker.getPosition().longitude));
                                        location.put("description", txtDesc.getText().toString());
                                        db.collection("favoris").add(location).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {
                                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                                            }
                                        })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w(TAG, "Error adding document", e);
                                                    }
                                                });
                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.rgb(0, 0, 0));
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(Color.rgb(0, 0, 0));

                return false;
            }
        });
    }


    private void geoLocate() {
        Log.d(TAG, "geoLocate: geolocate");

        String searchString = mSearch.getText().toString();

        Geocoder geocoder = new Geocoder(TestSplitMapStreetViewActivity.this);
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(searchString, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (list.size() > 0) {
            Address address = list.get(0);

            //Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show();
            moveCamera(new LatLng(address.getLatitude(), address.getLongitude()), 17, searchString);
        }
        hideSoftKeyboard();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLng paris = new LatLng(48.8, 2.3);
        /*mMap.addMarker(new MarkerOptions().position(paris).title("Marker in Paris"));*/
        mMap.moveCamera(CameraUpdateFactory.newLatLng(paris));
        enableMyLocation();
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                moveCamera(latLng, 13, "Custom location");
            }
        });
    }

    private void hideSoftKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
