package com.example.nvibetimothebertrandtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {

    private Button add, addLoc, hub, map, fav;

    /*private EditText address, locationName, locDesc;*/
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private FirebaseFirestore db;
    /*private boolean isDeve = false;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        //get firestore db instance
        db = FirebaseFirestore.getInstance();

        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };

        /*add = (Button) findViewById(R.id.btn_add);
        addLoc = (Button) findViewById(R.id.btn_addLocation);*/
        hub = (Button) findViewById(R.id.btn_hub);
        map = (Button) findViewById(R.id.btnMap);
        fav = (Button) findViewById(R.id.btn_fav);

        /*locationName = (EditText) findViewById(R.id.location_name);
        locDesc = (EditText) findViewById(R.id.locdesc);
        address = (EditText) findViewById(R.id.address);*/


        /*address.setVisibility(View.GONE);
        locDesc.setVisibility(View.GONE);
        locationName.setVisibility(View.GONE);
        add.setVisibility(View.GONE);*/


        /*addLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDeve) {
                    address.setVisibility(View.GONE);
                    locDesc.setVisibility(View.GONE);
                    locationName.setVisibility(View.GONE);
                    add.setVisibility(View.GONE);
                    isDeve = false;
                } else {
                    address.setVisibility(View.VISIBLE);
                    locDesc.setVisibility(View.VISIBLE);
                    locationName.setVisibility(View.VISIBLE);
                    add.setVisibility(View.VISIBLE);
                    isDeve = true;
                }
            }
        });*/

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FavTab.class));
            }
        });

        hub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HubActivity.class));
            }
        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        });

        /*add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }
}