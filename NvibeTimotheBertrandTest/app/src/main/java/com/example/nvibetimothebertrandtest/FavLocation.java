package com.example.nvibetimothebertrandtest;


//unused

public class FavLocation {
    private String email;
    private String name;
    private String address;
    private String desc;

    public FavLocation(String lEmail, String lName, String lAdress, String lDesc) {
        email = lEmail;
        name = lName;
        address = lAdress;
        desc = lDesc;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getDesc() {
        return desc;
    }

    public String getName() {
        return name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setName(String name) {
        this.name = name;
    }

}
