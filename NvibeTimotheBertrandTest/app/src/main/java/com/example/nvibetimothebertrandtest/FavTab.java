package com.example.nvibetimothebertrandtest;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.model.Document;

import java.util.ArrayList;

public class FavTab extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseUser curUser;
    private static final String TAG = FavTab.class.getName();

    private TextView txtLocname1, txtLoc1, txtLocDesc1,
            txtLocname2, txtLoc2, txtLocDesc2,
            txtLocname3, txtLoc3, txtLocDesc3,
            txtLocname4, txtLoc4, txtLocDesc4,
            txtLocname5, txtLoc5, txtLocDesc5;

    private Button btnDelLoc1, btnDelLoc2, btnDelLoc3,
            btnDelLoc4, btnDelLoc5,
            btnGoToLoc1, btnGoToLoc2,btnGoToLoc3,
            btnGoToLoc4, btnGoToLoc5;

    final ArrayList res = new ArrayList();

    CollectionReference fav = db.collection("favoris");

    private Button btnBackHub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_tab);

        curUser = FirebaseAuth.getInstance().getCurrentUser();

        btnBackHub = (Button) findViewById(R.id.btn_hub);

        txtLocname1 = (TextView) findViewById(R.id.txtLocName1);
        txtLoc1 = (TextView) findViewById(R.id.txtLocAddress1);
        txtLocDesc1 = (TextView) findViewById(R.id.txtLocDesc1);

        txtLocname2 = (TextView) findViewById(R.id.txtLocName2);
        txtLoc2 = (TextView) findViewById(R.id.txtLocAddress2);
        txtLocDesc2 = (TextView) findViewById(R.id.txtLocDesc2);

        txtLocname3 = (TextView) findViewById(R.id.txtLocName3);
        txtLoc3 = (TextView) findViewById(R.id.txtLocAddress3);
        txtLocDesc3 = (TextView) findViewById(R.id.txtLocDesc3);

        txtLocname4 = (TextView) findViewById(R.id.txtLocName4);
        txtLoc4 = (TextView) findViewById(R.id.txtLocAddress4);
        txtLocDesc4 = (TextView) findViewById(R.id.txtLocDesc4);

        txtLocname5 = (TextView) findViewById(R.id.txtLocName5);
        txtLoc5 = (TextView) findViewById(R.id.txtLocAddress5);
        txtLocDesc5 = (TextView) findViewById(R.id.txtLocDesc5);

        btnDelLoc1 = (Button) findViewById(R.id.btn_del_loc1);
        btnDelLoc2 = (Button) findViewById(R.id.btn_del_loc2);
        btnDelLoc3 = (Button) findViewById(R.id.btn_del_loc3);
        btnDelLoc4 = (Button) findViewById(R.id.btn_del_loc4);
        btnDelLoc5 = (Button) findViewById(R.id.btn_del_loc5);

        btnGoToLoc1 = (Button) findViewById(R.id.btn_access_loc1);
        btnGoToLoc2 = (Button) findViewById(R.id.btn_access_loc2);
        btnGoToLoc3 = (Button) findViewById(R.id.btn_access_loc3);
        btnGoToLoc4 = (Button) findViewById(R.id.btn_access_loc4);
        btnGoToLoc5 = (Button) findViewById(R.id.btn_access_loc5);

        final TextView[] txtViewList = {txtLocname1, txtLoc1, txtLocDesc1,
                txtLocname2, txtLoc2, txtLocDesc2,
                txtLocname3, txtLoc3, txtLocDesc3,
                txtLocname4, txtLoc4, txtLocDesc4,
                txtLocname5, txtLoc5, txtLocDesc5};

        final Button[] btnDelList = {btnDelLoc1, btnDelLoc2,
                btnDelLoc3, btnDelLoc4, btnDelLoc5};

        final Button[] btnGoToLocList = {btnGoToLoc1, btnGoToLoc2,
                btnGoToLoc3, btnGoToLoc4, btnGoToLoc5};

        for (int i = 0; i < txtViewList.length; i++) {
            txtViewList[i].setVisibility(View.GONE);
        }

        for (int i = 0; i < btnDelList.length; i++) {
            btnDelList[i].setVisibility(View.GONE);
            btnGoToLocList[i].setVisibility(View.GONE);
        }

        btnBackHub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FavTab.this, HubActivity.class));
            }
        });

        //first delete button onclick
        btnDelLoc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname1.getText().toString())
                        .whereEqualTo("description", txtLocDesc1.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                fav.document(documentSnapshot.getId()).delete();
                            }
                            reload();
                        } else {
                            Log.d(TAG, "error occured while deleting from database", task.getException());
                        }
                    }
                });
            }
        });

        //second delete button onclick
        btnDelLoc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname2.getText().toString())
                        .whereEqualTo("description", txtLocDesc2.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                fav.document(documentSnapshot.getId()).delete();
                            }
                            reload();
                        } else {
                            Log.d(TAG, "error occured while deleting from database", task.getException());
                        }
                    }
                });
            }
        });

        //third delete button onclick
        btnDelLoc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname3.getText().toString())
                        .whereEqualTo("description", txtLocDesc3.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                fav.document(documentSnapshot.getId()).delete();
                            }
                            reload();
                        } else {
                            Log.d(TAG, "error occured while deleting from database", task.getException());
                        }
                    }
                });
            }
        });

        //fourth delete button onclick
        btnDelLoc4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname4.getText().toString())
                        .whereEqualTo("description", txtLocDesc4.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                fav.document(documentSnapshot.getId()).delete();
                            }
                            reload();
                        } else {
                            Log.d(TAG, "error occured while deleting from database", task.getException());
                        }
                    }
                });
            }
        });

        //fifth delete button onclick
        btnDelLoc5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname5.getText().toString())
                        .whereEqualTo("description", txtLocDesc5.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                fav.document(documentSnapshot.getId()).delete();
                            }
                            reload();
                        } else {
                            Log.d(TAG, "error occured while deleting from database", task.getException());
                        }
                    }
                });
            }
        });

        //first go to location button
        btnGoToLoc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname1.getText().toString())
                        .whereEqualTo("description", txtLocDesc1.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Bundle b = new Bundle();
                        if (task.isSuccessful()) {
                            Intent goToIntent = new Intent(FavTab.this, MapsActivity.class);
                            GeoPoint geoPoint= (GeoPoint) task.getResult().getDocuments().get(0).get("location");
                            b.putDouble("Lat", geoPoint.getLatitude());
                            b.putDouble("Long", geoPoint.getLongitude());
                            goToIntent.putExtras(b);
                            startActivity(goToIntent);
                        } else {
                            Log.d(TAG, "error retrieving from database", task.getException());
                        }
                    }
                });
            }
        });

        //Second go to location button
        btnGoToLoc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname2.getText().toString())
                        .whereEqualTo("description", txtLocDesc2.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Bundle b = new Bundle();
                        if (task.isSuccessful()) {
                            Intent goToIntent = new Intent(FavTab.this, MapsActivity.class);
                            GeoPoint geoPoint= (GeoPoint) task.getResult().getDocuments().get(0).get("location");
                            b.putDouble("Lat", geoPoint.getLatitude());
                            b.putDouble("Long", geoPoint.getLongitude());
                            goToIntent.putExtras(b);
                            startActivity(goToIntent);
                        } else {
                            Log.d(TAG, "error retrieving from database", task.getException());
                        }
                    }
                });
            }
        });

        //third go to location button
        btnGoToLoc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname3.getText().toString())
                        .whereEqualTo("description", txtLocDesc3.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Bundle b = new Bundle();
                        if (task.isSuccessful()) {
                            Intent goToIntent = new Intent(FavTab.this, MapsActivity.class);
                            GeoPoint geoPoint= (GeoPoint) task.getResult().getDocuments().get(0).get("location");
                            b.putDouble("Lat", geoPoint.getLatitude());
                            b.putDouble("Long", geoPoint.getLongitude());
                            goToIntent.putExtras(b);
                            startActivity(goToIntent);
                        } else {
                            Log.d(TAG, "error retrieving from database", task.getException());
                        }
                    }
                });
            }
        });

        //fourth go to location button
        btnGoToLoc4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname4.getText().toString())
                        .whereEqualTo("description", txtLocDesc4.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Bundle b = new Bundle();
                        if (task.isSuccessful()) {
                            Intent goToIntent = new Intent(FavTab.this, MapsActivity.class);
                            GeoPoint geoPoint= (GeoPoint) task.getResult().getDocuments().get(0).get("location");
                            b.putDouble("Lat", geoPoint.getLatitude());
                            b.putDouble("Long", geoPoint.getLongitude());
                            goToIntent.putExtras(b);
                            startActivity(goToIntent);
                        } else {
                            Log.d(TAG, "error retrieving from database", task.getException());
                        }
                    }
                });
            }
        });

        //fifth go to location button
        btnGoToLoc5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query = fav.whereEqualTo("email", curUser.getEmail())
                        .whereEqualTo("name", txtLocname5.getText().toString())
                        .whereEqualTo("description", txtLocDesc5.getText().toString());

                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Bundle b = new Bundle();
                        if (task.isSuccessful()) {
                            Intent goToIntent = new Intent(FavTab.this, MapsActivity.class);
                            GeoPoint geoPoint= (GeoPoint) task.getResult().getDocuments().get(0).get("location");
                            b.putDouble("Lat", geoPoint.getLatitude());
                            b.putDouble("Long", geoPoint.getLongitude());
                            goToIntent.putExtras(b);
                            startActivity(goToIntent);
                        } else {
                            Log.d(TAG, "error retrieving from database", task.getException());
                        }
                    }
                });
            }
        });

        readData(new FirestoreCallback() {
            @Override
            public void onCallback(List<String> list) {
                Log.d(TAG, "dans le Firestore callback:" + list.toString());
                for (int i = 0; (i < list.size() && i < 15); i++) {
                    txtViewList[i].setText(list.get(i).toString());
                    txtViewList[i].setVisibility(View.VISIBLE);
                    if (i % 3 == 0) {
                        btnDelList[i / 3].setVisibility(View.VISIBLE);
                        btnGoToLocList[i / 3].setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void readData(final FirestoreCallback firestoreCallback) {
        Query query = fav.whereEqualTo("email", curUser.getEmail());
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot documentSnapshot : task.getResult()) {
                        String locName = (String) documentSnapshot.get("name");
                        String locPoint = documentSnapshot.get("location").toString();
                        String locDesc = (String) documentSnapshot.get("description");
                        res.add(locName);
                        res.add(locPoint);
                        res.add(locDesc);
                    }
                    /*Log.d(TAG, "Dans le readData:" + res.toString());*/
                    firestoreCallback.onCallback(res);
                } else {
                    Toast.makeText(FavTab.this, "No result found", Toast.LENGTH_LONG);
                }
            }
        });
    }

    public void reload() {
        finish();
        startActivity(getIntent());
    }

    private interface FirestoreCallback {
        void onCallback(List<String> list);
    }
}
